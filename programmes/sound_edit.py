import os
import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
import sounddevice as sd
import soundfile as sf
from scipy.io.wavfile import write
import pyqtgraph as pg
import os
import numpy as np
import matplotlib.pyplot as plt
from scipy import signal
from scipy.io import wavfile
from fft_et_ifft import *
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar

class FrequencySlider(QWidget):
        frequencies_edited_signal = pyqtSignal()

        def emit_frequencies_edited_signal(self):
            self.frequency_amplitude.setText(str(self.frequency_slider.value())+" db")
            self.frequencies_edited_signal.emit()

        def __init__(self, text="slider"):
            super().__init__()
            self.vlayout = QVBoxLayout()
            self.frequency_slider = QSlider(Qt.Vertical)
            self.frequency_slider.setMinimum(-40)
            self.frequency_slider.setMaximum(20)
            self.frequency_slider.setSliderPosition(0)
            self.frequency_slider.valueChanged.connect(self.emit_frequencies_edited_signal)
            self.frequency_label = QLabel(text)
            self.frequency_amplitude = QLabel("0 db")
            self.vlayout.addWidget(self.frequency_slider)
            self.vlayout.addWidget(self.frequency_label)
            self.vlayout.addWidget(self.frequency_amplitude)
            self.setLayout(self.vlayout)

class Window(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Sound Editor")
        self.resize(600, 400)

        self.record_fs = 44100

        self.tabs = QTabWidget()
        self.tab1 = QWidget()
        self.tab2 = QWidget()
        self.tab3 = QWidget()

        self.tabs.addTab(self.tab1,"Waveform")
        self.tabs.addTab(self.tab2,"Spectrogram")
        self.tabs.addTab(self.tab3,"Frequencies")

        self.mainlayout = QVBoxLayout(self)

        vlayout = QVBoxLayout()

        layout = QHBoxLayout()
        # Add widgets to the layout

        self.open_audio_button = QPushButton("import file")
        self.open_audio_button.clicked.connect(self.open_audio)
        layout.addWidget(self.open_audio_button)

        self.recording_button = QPushButton("record",)
        self.recording_button.clicked.connect(self.record)
        layout.addWidget(self.recording_button)

        label = QLabel("duration: ")
        label.setFixedWidth(55)
        layout.addWidget(label)

        self.duration_selector = QLineEdit("3")
        self.duration_selector.setFixedWidth(40)
        self.duration_selector.setValidator(QIntValidator())
        layout.addWidget(self.duration_selector)

        self.play_button = QPushButton("play")
        self.play_button.clicked.connect(self.play_sound)
        layout.addWidget(self.play_button)


        self.mainlayout.addLayout(layout)

        self.graphWidget = pg.PlotWidget()
        x = [0,1,2,3,4,5,6,7,8,9]
        y = [0,0,0,0,0,0,0,0,0,0]
        self.graphWidget.plot(x,y)

        vlayout.addWidget(self.graphWidget)

        self.vlayout2 = QVBoxLayout()
        self.figure = plt.figure()
        #plt.colormesh([[1,2],[3,4]])
        #plt.colorbar()
        self.canvas = FigureCanvas(self.figure)
        self.vlayout2.addWidget(self.canvas)
        #self.label = QLabel()
        #img = np.zeros((500, 500, 3), dtype=np.uint8)
        # Turn up red channel to full scale
        #img[...,0] = 255
        #qImg = QPixmap(QImage(img.data, img.shape[0], img.shape[1], QImage.Format_RGB888))
        #self.label.setPixmap(qImg)
        #self.vlayout2.addWidget(self.label)
        self.tab2.setLayout(self.vlayout2)

        self.vlayout3 = QVBoxLayout()
        self.hlayout_frequencies = QHBoxLayout()

        self.low_frequencies = FrequencySlider("0 hz")
        self.low_frequencies.frequencies_edited_signal.connect(self.modify_sound)
        self.lowmedium_frequencies = FrequencySlider("5000 hz")
        self.lowmedium_frequencies.frequencies_edited_signal.connect(self.modify_sound)
        self.medium_frequencies = FrequencySlider("10000 hz")
        self.medium_frequencies.frequencies_edited_signal.connect(self.modify_sound)
        self.mediumhigh_frequencies = FrequencySlider("15000 hz")
        self.mediumhigh_frequencies.frequencies_edited_signal.connect(self.modify_sound)
        self.high_frequencies = FrequencySlider("20000 hz")
        self.high_frequencies.frequencies_edited_signal.connect(self.modify_sound)

        self.hlayout_frequencies.addWidget(self.low_frequencies)
        self.hlayout_frequencies.addWidget(self.lowmedium_frequencies)
        self.hlayout_frequencies.addWidget(self.medium_frequencies)
        self.hlayout_frequencies.addWidget(self.mediumhigh_frequencies)
        self.hlayout_frequencies.addWidget(self.high_frequencies)

        self.vlayout3.addLayout(self.hlayout_frequencies)

        self.pitch_slider = QSlider(Qt.Horizontal)
        self.pitch_slider.setMinimum(-self.record_fs)
        self.pitch_slider.setMaximum(self.record_fs)
        self.pitch_slider.valueChanged.connect(self.modify_sound)
        self.vlayout3.addWidget(self.pitch_slider)

        self.reset_frequencies_button = QPushButton("reset")
        self.reset_frequencies_button.clicked.connect(self.reset_frequencies)
        self.vlayout3.addWidget(self.reset_frequencies_button)

        self.tab3.setLayout(self.vlayout3)
        
        # Set the layout on the application's window
        self.tab1.setLayout(vlayout)
        self.mainlayout.addWidget(self.tabs)
        self.setLayout(self.mainlayout)

    def reset_frequencies(self):
        self.low_frequencies.frequency_slider.setSliderPosition(0)
        self.lowmedium_frequencies.frequency_slider.setSliderPosition(0)
        self.medium_frequencies.frequency_slider.setSliderPosition(0)
        self.mediumhigh_frequencies.frequency_slider.setSliderPosition(0)
        self.high_frequencies.frequency_slider.setSliderPosition(0)

    def redraw_spectrogram(self):
        self.vlayout2.removeWidget(self.canvas)
        plt.close(self.figure)
        self.figure = plt.figure()
        self.canvas = FigureCanvas(self.figure)
        self.vlayout2.addWidget(self.canvas)
        #plt.pcolormesh(np.log(np.abs(fast_stft_triang_window(self.edited_sound, 2048))))
        plt.pcolormesh(10*np.log10(np.abs(signal.stft(self.edited_sound)[2])))
        plt.colorbar()

    def redraw_waveform(self):
        self.graphWidget.clear()
        self.graphWidget.plot(range(len(self.edited_sound)),self.edited_sound)
    
    def open_audio(self):
        name = QFileDialog.getOpenFileName(self, 'Open File', os.getcwd(), "Wav audio (*.wav)")
        if name[0] != "":
            self.recording, self.record_fs = sf.read(name[0])
            self.edited_sound = self.recording
            self.redraw_waveform()
            self.reset_frequencies()
            self.redraw_spectrogram()


    def modify_sound(self):
            f, t, Zxx = signal.stft(self.recording, nperseg=2048)
            range_frequencies = int((Zxx.shape[0])*20000/self.record_fs//1)
            r = range_frequencies//4
            for i in range(0,r):
                oldZxx = Zxx
                try:
                    Zxx[i,:] = 10**(((r-i)/r*self.low_frequencies.frequency_slider.value() + i/r*self.lowmedium_frequencies.frequency_slider.value())/10)*Zxx[i,:]
                except:
                    break
            q = r
            r = range_frequencies//2
            s = r-q
            for i in range(q,r):
                try:
                    Zxx[i,:] = 10**(((r-i)/s*self.lowmedium_frequencies.frequency_slider.value() + (i-q)/s*self.medium_frequencies.frequency_slider.value())/10)*Zxx[i,:]
                except:
                    break
            q = r
            r = range_frequencies*3//4
            s = r-q
            for i in range(q,r):
                try:
                    Zxx[i,:] = 10**(((r-i)/s*self.medium_frequencies.frequency_slider.value() + (i-q)/s*self.mediumhigh_frequencies.frequency_slider.value())/10)*Zxx[i,:]
                except:
                    break
            q = r
            r = range_frequencies
            s = r-q
            for i in range(q,r):
                try:
                    Zxx[i,:] = 10**(((r-i)/s*self.mediumhigh_frequencies.frequency_slider.value() + (i-q)/s*self.high_frequencies.frequency_slider.value())/10)*Zxx[i,:]
                except:
                    break

            # change pitch
            a = int(self.pitch_slider.value()/self.record_fs*Zxx.shape[0]//100)
            if a > 0:
                for i in range(a):
                    Zxx = np.insert(Zxx,0,[np.array([0]*Zxx.shape[1])],axis=0)
                    Zxx = np.delete(Zxx, Zxx.shape[0]-1, axis=0)
            else:
                for i in range(a):
                    Zxx = np.insert(Zxx,Zxx.shape[0],[np.array([0]*Zxx.shape[1])],axis=0)
                    Zxx = np.delete(Zxx, 0, axis=0)

            t, x = signal.istft(Zxx, nperseg=2048)
            self.edited_sound = x[:self.edited_sound.shape[0]]
            self.redraw_waveform()
            self.redraw_spectrogram()


    def record(self):
        self.recording = sd.rec(int(self.duration_selector.text()) * self.record_fs, samplerate=self.record_fs, channels=1)[:,0]
        sd.wait()
        self.edited_sound = self.recording
        self.redraw_waveform()
        self.reset_frequencies()
        self.redraw_spectrogram()

    def play_sound(self):
        sd.play(self.edited_sound, self.record_fs)

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = Window()
    window.show()
    sys.exit(app.exec_())
