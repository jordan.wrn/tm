import sys
from PyQt5 import QtCore, QtGui, QtWidgets, uic
from PyQt5.QtCore import Qt
import numpy as np
import math
from fft_et_ifft import *
import threading
import time

class Canvas(QtWidgets.QLabel):

    def __init__(self):
        super().__init__()
        pixmap = QtGui.QPixmap(input("quel fichier?: "))
        pixmap = pixmap.scaled(QtCore.QSize(400,400))
        self.background_pixmap = pixmap
        self.setPixmap(pixmap)
        self.pen_color = QtGui.QColor('#000000')

        self.points_to_interpolate = []
        self.factor_more_points = 16 #le facteur de points supplémentaires qu'on utilise pour dessiner la courbe
        self.points_for_curve = []

    def mousePressEvent(self, e):
        self.points_to_interpolate += [complex(e.x(), e.y())]
        painter = QtGui.QPainter(self.pixmap())
        pen = QtGui.QPen()
        pen.setWidth(4)
        pen.setColor(self.pen_color)
        painter.setPen(pen)
        painter.drawPoint(e.x(), e.y())
        painter.end()
        self.update()

    def clear_canvas():
        self.setPixmap(self.background_pixmap)
        self.repaint()
        self.update()
        self.points_to_interpolate = []
        self.points_for_curve = []

    def draw_curve(self):
        length = len(self.points_to_interpolate)
        if length > 1:
            self.points_for_curve = ifft(fft(self.points_to_interpolate)[:length//2]+[0]*((self.factor_more_points-1)*length)+fft(self.points_to_interpolate)[length//2:])
        for i in range(len(self.points_for_curve)):
            painter = QtGui.QPainter(self.pixmap())
            pen = QtGui.QPen()
            pen.setWidth(2)
            pen.setColor(self.pen_color)
            painter.setPen(pen)
            painter.drawLine(round(self.points_for_curve[i-1].real),round(self.points_for_curve[i-1].imag),round(self.points_for_curve[i].real),round(self.points_for_curve[i].imag))
            painter.end()
            self.update()

    def rotating_arrows(self):
        arrows = fft(self.points_to_interpolate)
        a = self.factor_more_points
        for t in range(len(arrows)*a): #dans le temps
            self.setPixmap(self.background_pixmap)
            p1 = arrows[0]
            for i in range(1, len(arrows)): #dessiner la somme des flèches
                

                # la manière suivante dessine d'abord toutes les flèches tournantes d'un sens,
                #puis toutes celles dans l'autre.
                # if i < len(arrows)//2:
                #     p2 = p1 + arrows[i]*np.exp(2j*np.pi*(t/32)*i/len(arrows))
                # else:
                #     p2 = p1 + arrows[i]*np.exp(2j*np.pi*(t/32)*(i-len(arrows))/len(arrows))

                # cette manière altèrne entre une flèche tournant dans un sens puis l'autre dans l'autre,
                #sauf dans certains cas à la fin
                # flèche sens trigonométrique
                if i < len(arrows)//2:
                    painter = QtGui.QPainter(self.pixmap())
                    pen = QtGui.QPen()
                    pen.setWidth(2)
                    pen.setColor(self.pen_color)
                    painter.setPen(pen)
                    p2 = p1 + arrows[i]*np.exp(2j*np.pi*(t/a)*i/len(arrows))
                    painter.drawLine(round(p1.real),round(p1.imag),round(p2.real),round(p2.imag))
                    painter.end()
                    p1 = p2

                # flèche sens antitrigonométrique
                if len(arrows)-i >= len(arrows)//2:
                    painter = QtGui.QPainter(self.pixmap())
                    pen = QtGui.QPen()
                    pen.setWidth(2)
                    pen.setColor(self.pen_color)
                    painter.setPen(pen)
                    p2 = p1 + arrows[-i]*np.exp(2j*np.pi*(t/a)*(-i)/len(arrows))
                    painter.drawLine(round(p1.real),round(p1.imag),round(p2.real),round(p2.imag))
                    painter.end()
                    p1 = p2
                
           
            self.draw_curve()
            self.repaint()
            time.sleep(0.05)
        self.setPixmap(self.background_pixmap)
        painter = QtGui.QPainter(self.pixmap())
        pen = QtGui.QPen()
        pen.setWidth(4)
        pen.setColor(self.pen_color)
        painter.setPen(pen)
        for i in self.points_to_interpolate:
            painter.drawPoint(round(i.real),round(i.imag))
        painter.end()

                    
                

COLORS = [
# 17 undertones https://lospec.com/palette-list/17undertones
'#000000', '#141923', '#414168', '#3a7fa7', '#35e3e3', '#8fd970', '#5ebb49',
'#458352', '#dcd37b', '#fffee5', '#ffd035', '#cc9245', '#a15c3e', '#a42f3b',
'#f45b7a', '#c24998', '#81588d', '#bcb0c2', '#ffffff',
]


class QPaletteButton(QtWidgets.QPushButton):

    def __init__(self, color):
        super().__init__()
        self.setFixedSize(QtCore.QSize(24,24))
        self.color = color
        self.setStyleSheet("background-color: %s;" % color)

class MainWindow(QtWidgets.QMainWindow):

    def __init__(self):
        super().__init__()

        self.canvas = Canvas()

        w = QtWidgets.QWidget()
        l = QtWidgets.QVBoxLayout()
        w.setLayout(l)
        l.addWidget(self.canvas)

        palette = QtWidgets.QHBoxLayout()
        self.add_palette_buttons(palette)
        l.addLayout(palette)

        self.setCentralWidget(w)

    def add_palette_buttons(self, layout):
        for c in COLORS:
            b = QPaletteButton(c)
            b.pressed.connect(lambda c=c: self.canvas.set_pen_color(c))
            layout.addWidget(b)
        play_button = QtWidgets.QPushButton("play")
        play_button.clicked.connect(self.canvas.rotating_arrows)
        layout.addWidget(play_button)
        clear_button = QtWidgets.QPushButton("clear")
        clear_button.clicked.connect(self.canvas.clear)
        layout.addWidget(clear_button)


app = QtWidgets.QApplication(sys.argv)
window = MainWindow()
window.show()
app.exec_()

