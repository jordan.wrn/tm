import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from scipy.io.wavfile import read,write
matplotlib.use("pgf")
matplotlib.rcParams.update({
"pgf.texsystem": "xelatex",
'font.family': 'serif',
'text.usetex': True,
'pgf.rcfonts': False,
})
fichier = input("tappez le nom d'un fichier wav: ")
Fs, data = read(fichier)
data = data[:,0]
data = np.append(data,[0]*int(2**(np.ceil(np.log2(len(data))))-len(data)))
N = len(data)
# print(len(data))
# print(int(np.floor(np.log2(len(data)))))
# print(int(2**(np.floor(np.log2(len(data))))))
# print(np.exp(-2j*np.pi*1/2))
# N=4
# data = [1,np.complex(0,1),-1,np.complex(0,-1)]
def fft(x):
    if len(x) == 1:
        return [x[0]]
    else:
        result = [0]*len(x)
        a = fft(x[::2])
        b = fft(x[1::2])
        for k in range(-len(x)//2,0):
            result[k] = a[k]/2 + np.exp(-2j*np.pi*k/len(x))*b[k]/2
            result[k+len(x)//2] = a[k]/2 - np.exp(-2j*np.pi*k/len(x))*b[k]/2
        return result
def ifft(y):
    if len(y) == 1:
        return [y[0]]
    else:
        result = [0]*len(y)
        a = ifft(y[::2])
        b = ifft(y[1::2])
        for k in range(0,len(y)//2):
            result[k] = a[k] + np.exp(2j*np.pi*k/len(y))*b[k]
            result[k+len(y)//2] = a[k] - np.exp(2j*np.pi*k/len(y))*b[k]
        return result
transformed = fft(data)
# for i in range(5000,len(transformed)):
#     transformed[i] = 0
data = ifft(transformed)
amplitudes = [0]*len(transformed)
angles = [0]*len(transformed)
for k in range(N):
    #data[k] = np.abs(data[k])
    amplitudes[k] = np.abs(transformed[k])
    angles[k] = np.angle(transformed[k])
amplitudes = amplitudes[:len(amplitudes)//2]
angles = angles[:len(angles)//2]
fig, axs = plt.subplots(2)
#fig.suptitle("Nothing")
axs[0].plot(data)
axs[1].plot(amplitudes,'tab:orange')
#axs[2].plot(angles,'tab:green')
#plt.xlabel("Sample Index")
#plt.ylabel("Amplitude")
#plt.title("Waveform of Test Audio")
plt.show()
plt.savefig("son.pgf")
