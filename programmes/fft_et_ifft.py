import numpy as np

def fft(x):
    if len(x) == 1:
        return np.array([x[0]])
    elif len(x) % 2 == 0:
        result = [0]*len(x)
        a = fft(x[::2])
        b = fft(x[1::2])
        for k in range(-len(x)//2,0):
            result[k] = a[k]/2 + np.exp(-2j*np.pi*k/len(x))*b[k]/2
            result[k+len(x)//2] = a[k]/2 - np.exp(-2j*np.pi*k/len(x))*b[k]/2
        return result
    else: # transformation lente
        result = [0]*len(x)
        for k in range(len(x)):
            for l in range(len(x)):
                result[k] += np.exp(-2j*np.pi*k*l/len(x)) * x[l] / len(x)
        return result

def ifft(y):
    if len(y) == 1:
        return np.array([y[0]])
    elif len(y) % 2 == 0:
        result = [0j]*len(y)
        a = ifft(y[::2])
        b = ifft(y[1::2])
        for k in range(-len(y)//2,0):
            result[k] = a[k] + np.exp(2j*np.pi*k/len(y))*b[k]
            result[k+len(y)//2] = a[k] - np.exp(2j*np.pi*k/len(y))*b[k]
        return result
    else: # transformation lente
        result = [0j] * len(y)
        for k in range(len(y)):
            for l in range(len(y)):
                result[k] += y[l] * np.exp(2j*np.pi*k*l/len(y))
        return result

def fast_stft_triang_window(x, window_size):
    result = np.array([[0j] * window_size] * (2*len(x)//window_size))
    a = window_size//2
    for i in range(2*len(x)//window_size):
        x_after_windowing = [0] * window_size
        for j in range(window_size//2):
            x_after_windowing[j] = j * x[j - window_size//2 + i*a] 
            x_after_windowing[window_size-1-j] = (j+1) * x[window_size//2 - 1 - j + i*a]
        result[i] = fft(x_after_windowing)
    return result

def fast_istft_triang_window(y):
    window_size = len(y[0])
    result_1 = np.array([[0j] * window_size] * len(y))
    for i in range(len(result_1)):
        result_1[i] = ifft(y[i])
    result_2 = [0] * (len(y)*window_size//2)
    for i in range(len(result_1)):
        for j in range(window_size):
            result_2[j + (i-1)*window_size//2] += result_1[i][j].real/window_size*2 
    return result_2
