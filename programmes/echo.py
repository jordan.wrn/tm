import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from scipy.io.wavfile import read,write
matplotlib.use("pgf")
matplotlib.rcParams.update({
"pgf.texsystem": "pdflatex",
'font.family': 'serif',
'text.usetex': True,
'pgf.rcfonts': False,
})
fichier = input("tappez le nom d'un fichier wav: ")
Fs, data = read(fichier)
data = data[:,0]
d1 = len(data)
abcd = data
#data[0:100000] = [250]*100000
#write("test.wav",Fs,data)
print(type(data),len(data))
data = np.append(data,[0]*int(2**(np.ceil(np.log2(len(data))))-len(data)),axis=None)
N = len(data)
echo = [0]*N
convolution_transformed = [0]*N
#for i in range(N-d1):
    #echo[i] = (1+np.sin(i/2000)/1.5)*np.exp(-i/Fs/8)/30
echo[0] = 0.3
echo[3000] = 0.25
echo[6000] = 0.2
echo[9000] = 0.15
echo[12000] = 0.1
echo[15000] = 0.05
# print(len(data))
# print(int(np.floor(np.log2(len(data)))))
# print(int(2**(np.floor(np.log2(len(data))))))
# print(np.exp(-2j*np.pi*1/2))
# N=4
# data = [1,np.complex(0,1),-1,np.complex(0,-1)]
def fft(x):
    if len(x) == 1:
        return [x[0]]
    else:
        result = [0]*len(x)
        a = fft(x[::2])
        b = fft(x[1::2])
        for k in range(-len(x)//2,0):
            result[k] = a[k]/2 + np.exp(-2j*np.pi*k/len(x))*b[k]/2
            result[k+len(x)//2] = a[k]/2 - np.exp(-2j*np.pi*k/len(x))*b[k]/2
        return result
def ifft(y):
    if len(y) == 1:
        return [y[0]]
    else:
        result = [0]*len(y)
        a = ifft(y[::2])
        b = ifft(y[1::2])
        for k in range(0,len(y)//2):
            result[k] = a[k] + np.exp(2j*np.pi*k/len(y))*b[k]
            result[k+len(y)//2] = a[k] - np.exp(2j*np.pi*k/len(y))*b[k]
        return result
transformed = fft(data)
transformed_echo = fft(echo)
amplitudes_echo = [0]*len(transformed_echo)
amplitudes_convolution = [0]*len(convolution_transformed)
# for i in range(5000,len(transformed)):
#     transformed[i] = 0
echo = ifft(transformed_echo)
#data = ifft(transformed)
amplitudes = [0]*len(transformed)
angles = [0]*len(transformed)
for k in range(N):
    #data[k] = np.abs(data[k])
    amplitudes[k] = np.abs(transformed[k])
    amplitudes_echo[k] = np.abs(transformed_echo[k])
    angles[k] = np.angle(transformed[k])
    convolution_transformed[k] = N*transformed[k]*transformed_echo[k]
    amplitudes_convolution[k] = np.abs(convolution_transformed[k])
convolution = ifft(convolution_transformed)
amplitudes = amplitudes[:len(amplitudes)//2]
amplitudes_echo = amplitudes_echo[:len(amplitudes_echo)//2]
amplitudes_convolution = amplitudes_convolution[:len(amplitudes_convolution)//2]
angles = angles[:len(angles)//2]
convolution = np.real(convolution)
#write("echo.wav",Fs,convolution[0:100000])
data = np.real(data)
abcd[:d1] = convolution[:d1]
write("echo.wav",Fs,abcd)
fig, axs = plt.subplots(6)
fig.tight_layout()
axs[0].plot(data)
axs[0].set_title("son: pression au cours du temps")
axs[1].plot(echo)
axs[1].set_title("echo: répétitions du son à des amplitudes moins fortes après des delais")
axs[2].plot(amplitudes,'tab:orange')
axs[2].set_title("transformée de Fourier du son")
axs[3].plot(amplitudes_echo,'tab:orange')
axs[3].set_title("transformée de Fourier de la fonction de l'écho")
axs[4].plot(amplitudes_convolution,'tab:green')
axs[4].set_title("multiplication point par point des transformées de Fourier")
axs[5].plot(convolution,'tab:red')
axs[5].set_title("transformée inverse de multiplication")
#plt.xlabel("Sample Index")
#plt.ylabel("Amplitude")
#plt.title("Waveform of Test Audio")
plt.show()
plt.savefig("echo.pgf")
#write("test.wav",Fs,convolution[:d1])
