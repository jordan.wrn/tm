\section{Transformation de Fourier}
\subsection{Utilité}
La transformation de Fourier décompose une fonction en somme
de fonctions sinusoïdales (ou exponentielles complexes dans le cas complexe),
comme par exemple l'expression d'un accord musical en termes de volumes et fréquences
de ses notes. Elle offre ainsi une nouvelle représentation des données en fonction de fréquences,
au lieu d'une représentation temporale, qui est parfois moins facile à interpréter.

\begin{figure}[ht]
  \centering
\begin{tikzpicture}
\pgfplotsset{ticks=none}
\begin{axis}[
    xmin = 0, xmax = 15,
    ymin = -1.5, ymax = 1.5,
    %minor tick num = 1,
    major grid style = {lightgray},
    minor grid style = {lightgray!25},
    width = \textwidth,
    height = 45mm]
    \addplot[
    domain = 0:30,
        samples = 200,
        smooth,
        thick,
        black,
        ] {0.4*sin(666*x/2/pi) - 0.4*sin(1524*x/2/pi) + 0.4*sin(2836*x/2/pi)};
    \addplot[
    domain = 0:30,
        samples = 200,
        smooth,
        thin,
        red!70,
        ] {0.4*sin(666*x/2/pi)};
     \addplot[
    domain = 0:30,
        samples = 200,
        smooth,
        thin,
        green!70,
        ] {0.4*sin(1524*x/2/pi)};
    \addplot[
    domain = 0:30,
        samples = 200,
        smooth,
        thin,
        blue!70,
    ] {0.4*sin(2836*x/2/pi)};
\end{axis}
 
\end{tikzpicture}
\caption{Un accord parfait majeur \textsc{do - mi - sol} (en noir) joué par un générateur d'ondes, comme somme de ses notes (en couleurs). La représentation en noir est difficilement interprétable à l'oeil, alors qu'avec
  la représentation de Fourier, on peut facilement visualiser quelles notes sont jouées.}
\end{figure}

Elle est utilisée dans de nombreux domaines, comme le traitement d'images et de sons,
la résolution d'équations différencielles, la spectroscopie, la mécanique quantique,
le traitement de signaux et beaucoup d'autres choses.

Avant de se plonger dans les équations, voici une analogie \footnote{Trouvée sur \cite{fourieranalogy}}
qui peut permettre de mieux
comprendre l'utilité de la transformation de Fourier:
\begin{itemize}
\item Étant donné un smoothie,
la transformatation de Fourier permet de nous donner sa recette.
\item Pour ce faire,
elle fait passer le smoothie par des filtres qui extraient les différents ingrédients.
\item Les recettes sont beaucoup plus facile à analyser et modifier que le smoothie lui-même.
\item Pour recréer le smoothie, on mélange simplement les ingrédients, ce qui représente
  la transformation de Fourier inverse.
\end{itemize}


\newpage
\subsection{Représentation et calcul des coefficients}

Pour mettre en pratique cette transformation dans toute sa généralité,
j'ai commencé par une application sympatique: le dessin d'une courbe quelquonque
(par exemple le contour d'un objet)
passant par des points situés dans le plan complèxe grâce à des flèches tournantes de différentes
tailles et vitesses de rotation. Ces flèches tournantes peuvent être représentées
dans le plan complèxe comme:
\[\lambda_k e^{ik \frac{t}{p} 2\pi} = \lambda_k \pa{\cos\pa{k \frac{t}{p} 2\pi} + i \cdot \sin\pa{k \frac{t}{p} 2\pi}}
\text{ avec } t \in [0,p]\]
où $\lambda_k \in \mathbb{C}$ est la position initiale, $k \in \mathbb{Z}$ le nombre de rotations par periode
$p$ qui est le temps qu'il faut pour dessiner la courbe.
La partie imaginaire va être l'ordonnée et la partie réele l'abscisse.

\begin{figure}[h!t]
  \centering
\resizebox{.5\textwidth}{!}{%
\begin{tikzpicture}
    \begin{scope}[thick,font=\normalsize]
    \draw [color=gray] (0,0) circle (5);
    \draw [->] (-6,0) -- (6,0) node [above left]  {$\mathsf{Re}$};
    \draw [->] (0,-6) -- (0,6) node [below right] {$\mathsf{Im}$};

    
    \foreach \n in {-5,...,-1,1,2,...,5}{%
        \draw (\n,-3pt) -- (\n,3pt)   node [above] {$\n$};
        \draw (-3pt,\n) -- (3pt,\n)   node [right] {$\n i$};}
        
    \draw [-{Latex[width=3mm,length=5mm]},thick, color=red] (0,0) -- (3,4);
    \draw [color=blue, fill=blue] (3,4) circle(0.05);
    \node [color=black] at (4.0,4.2) {$\lambda_k = 3 + 4i$};
\end{scope}
\end{tikzpicture}}
\caption{Trajectoire d'une flèche tournante avec sa position initiale en rouge}
\end{figure}

Supposons qu'il y a $N$ points dans le plan complèxe (nommés $x_0$, $\dots$, $x_{N-1}$) par lesquels la courbe
doit passer, et choisons la periode $p = N$. On veut que $f(0) = x_0$, $\dots$, $f(N-1) = x_{N-1}$. Supposons
 - avant de le montrer plus tard à la page \pageref{interpolation} - qu'il est bien possible de dessiner une
courbe passant par ces $N$ points grâce à la somme de $N$ flèches tournantes:
$$f(t) = \sum_{k=\sfrac{-N}{2}}^{\sfrac{N}{2}-1} \lambda_k e^{i k \frac{t}{N} 2\pi}$$
Pour $k>0$, la flèche tourne dans le sens trigonométrique, pour $k=0$ la flèche ne tourne pas,
et pour $k<0$ la flèche tourne dans le sens des aiguilles d'une montre.

Remarque: Les indices de cette somme peuvent être choisis arbitrairement,
pour autant qu'il y ait une somation de $N$ éléments consécutifs.
Souvent sur les ressources décrivant la transformation de Fourier,
on trouve la sommation $\sum_{k=0}^{N-1}$, avec seulement des flèches tournantes dans le sens trigonométrique.

La courbe obtenue en combinant certaines flèches tournantes dans le sens trigonométrique
et d'autres flèches tournantes dans le sens des aiguilles a une apparence moins bouclée, comme j'essaie d'illustrer ci-dessous. C'est la raison pourquoi j'ai préféré utiliser cette version dans mon programme.

\begin{figure}[h]
  \centering
  \resizebox{\textwidth}{!}{%
    \begin{tikzpicture}
      \pgfplotsset{ticks=none}
  \begin{axis}[axis equal]
    \addplot[domain=0:8,samples=100,thick,color=blue]
    ({1/8*4*(1+sqrt(2))*cos(deg(x/8*2*pi)) + 1/8*4*(1-sqrt(2))*cos(deg(x/8*(-3)*2*pi))},
    {1/8*4*(1+sqrt(2))*sin(deg(x/8*2*pi)) + 1/8*4*(1-sqrt(2))*sin(deg(x/8*(-3)*2*pi))});
    \addplot[domain=0:8,samples=100,thick,color=green]
    ({1/8*4*(1+sqrt(2))*cos(deg(x/8*2*pi)) + 1/8*4*(1-sqrt(2))*cos(deg(x/8*(5)*2*pi))},
    {1/8*4*(1+sqrt(2))*sin(deg(x/8*2*pi)) + 1/8*4*(1-sqrt(2))*sin(deg(x/8*(5)*2*pi))});
    \draw [-{Latex[width=2mm,length=2mm]},thick, color=red] (0,0) -- (1.148,0.373);
    \draw [-{Latex[width=2mm,length=2mm]},thick, color=red] (1.148,0.373) -- (1.026,0.54);
    \draw [-{Latex[width=2mm,length=2mm]},thick, color=orange] (0,0) -- (1.119,-0.452);
    \draw [-{Latex[width=2mm,length=2mm]},thick, color=orange] (1.119,-0.452) -- (1.19,-0.257);
  \end{axis}
\end{tikzpicture}}
\caption{\label{fig:sensopposésvsunsens}Exemple: tracé approximatif d'un carré à l'aide de flèches tournantes. En bleu
  avec deux flèches tournantes de sens opposé, en vert avec deux flèches tournantes
de même sens. Certaines des flèches ont une amplitude nulle.}
\end{figure}
\newpage

Nous connaisons les points par où la courbe doit passer, mais ne savons pas encore explicitement les $\lambda_j$ (avec $j \in {-\sfrac{N}{2}, \dots, \sfrac{N}{2}}$). Pour les déterminer, on utilise l'orthogonalité, en définissant
$\omega_{N} := e^{\frac{-2\pi i}{N}}$:
%% \begin{figure}
%% \resizebox{\columnwidth}{!}{%
%% \begin{tikzpicture}
%%     \begin{scope}[thick,font=\tiny]
%%     \draw [color=gray] (0,0) circle (1);
%%     \draw [->] (-2,0) -- (2,0) node [above left]  {$\mathsf{Re}$};
%%     \draw [->] (0,-2) -- (0,2) node [below right] {$\mathsf{Im}$};

    
%%     % \foreach \n in {-1,1}{%
%%     %     \draw (\n,-3pt) -- (\n,3pt)   node [above] {$\n$};
%%     %     \draw (-3pt,\n) -- (3pt,\n)   node [right] {$\n i$};}
        
%%     \foreach \n in {0,45,90,135,180,225,270,315}{%
%%       \draw [color=blue, fill=blue] (\n:1) circle(0.05);}
%%     \draw [color=red, fill=red] (0,0) circle(0.05);
      
%% \end{scope}
%% \end{tikzpicture}}
%% \caption{Si $k \neq j$, alors les points obtenus en variant $m$ sont répartis autour du cercle, avec une moyenne nulle.}
%% \end{figure}

\begin{equation*}
  \begin{split}
  \frac{1}{N} \sum_{m=0}^{N-1} \omega_N^{jm} f(m) \\
= \frac{1}{N} \sum_{m=0}^{N-1} \omega_N^{jm} \sum_{k=\sfrac{-N}{2}}^{\sfrac{N}{2}-1} \lambda_k \omega_N^{-km} \\
= \sum_{k=\sfrac{-N}{2}}^{\sfrac{N}{2}-1} \lambda_k \underbrace{\frac{1}{N} \sum_{m=0}^{N-1} \underbrace{\omega_N^{(j-k)m}}_{= 1 \text{ si } k = j \text{\tiny (mod $N$)}}}_{\begin{cases} =1 \text{ si } k = j \text{ \tiny (mod $N$)}\\=0 \text{ si } k \neq j \text{ \tiny (mod $N$)}\end{cases}}
= \sum_{k=\sfrac{-N}{2}}^{\sfrac{N}{2}-1} \lambda_k \delta_{kj} = \lambda_j
\end{split}
\end{equation*}
Car si $k \neq j$ (mod $N$), alors $1 -\omega^{j-k} \neq 0$, donc en multipliant par
$\dfrac{1 -\omega^{j-k}}{1 -\omega^{j-k}}$,
on obtient:
\[ \sum_{m=0}^{N-1} \omega^{(j-k)m} = \frac{1 - \omega^{(j-k)N}}{1 - \omega^{j-k}}
= \frac{0}{1 - \omega^{j-k}} = 0\]

Notons \label{renotation} ce que nous avions noté $\lambda_j$ comme: \[\displaystyle\pa{\mathcal{F}_N\pmat{x_0 \\ \vdots \\ x_{N-1}}}_j
  = \frac{1}{N} \sum_{m=0}^{N-1} \omega_N^{jm} x_m  \text{ avec }
  j \in \{\sfrac{-N}{2},\dots,\sfrac{N}{2}-1\}\]

  Nous montrons maintenant que la courbe utilisant ces $\lambda_j$ passe bien
  par les points $x_0$, $\dots$, $x_{N-1}$:

\begin{theorem}[Interpolation] \label{interpolation} Soit les points $x_0,\dots,x_{N-1}$ Alors
      \begin{equation*} \begin{aligned}\sum_{k=\sfrac{-N}{2}}^{\sfrac{N}{2}-1}
        \pa{\mathcal{F}_N\pmat{x_0 \\ \Vdots \\ x_{N-1}}}_ke^{ik\frac{t}{N}2\pi} = x_t\\
      \forall t \in \{0,\dots,N-1\}\end{aligned}\end{equation*}
\end{theorem}
\begin{proofj}
  Toujours grâce à l'orthogonalité:
  \begin{equation*}
    \begin{aligned}
      &\sum_{k=\sfrac{-N}{2}}^{\sfrac{N}{2}-1} \frac{1}{N} \sum_{m=0}^{N-1} \omega_N^{km} x_m e^{ik\frac{t}{N}2\pi} \\
       &= \sum_{m=0}^{N-1} \underbrace{\frac{1}{N} \sum_{k=\sfrac{-N}{2}}^{\sfrac{N}{2}-1} e^{ik\frac{t-m}{N}2\pi}}_
       {\begin{cases} = 1 \text{ si }m=t\\ = 0 \text{ si }m\neq t \end{cases}}x_m \\
       &= \sum_{m=0}^{N-1} \delta_{mt} x_m = x_t
    \end{aligned}
    \end{equation*}
\end{proofj}

\subsection{Temps de calcul}
En notation matricielle nous pouvons écrire:
%% \sidenotetext[a]{\small\textbf{Remarque:} Souvent on trouve la notation:
%%   $\displaystyle x_t = f(t) = \sum_{k=0}^{N-1} \lambda_k e^{ik\frac{t}{N}2\pi}$ \newline
%%   Là, on utilise seulement des flèches tournantes dans le sens trigonométrique. Pour des valeurs de $t$
%%   entières on obtient le même résultat: \newline
%%   $\displaystyle \frac{e^{ik\frac{t}{N}2\pi}}{1}
%%   = \frac{e^{ik\frac{t}{N}2\pi}}{e^{iN\frac{t}{N}2\pi}}
%%   = e^{i\overbrace{(k-N)}^{<0} \frac{t}{N}2\pi}$ \newline
%%   Cependant, pour les valeurs de $t$ non entières,
%%   la trajectoire fait des boucles comme on le voit sur la figure
%%   \ref{fig:sensopposésvsunsens}.
%%   C'est pourquoi j'ai choisi de ne pas utiliser cette notation.}

%%   \begin{figure}
%%   \resizebox{\columnwidth}{!}{%
%% \begin{tikzpicture}[thick,font=\tiny]
%%   \draw [->] (-2,0)--(2,0)node[above left]{$\mathsf{Re}$};
%%   \draw [->] (0,-2)--(0,2)node[below right]{$\mathsf{Im}$};
    
%%       \draw[
%%       color=gray
%%       ]
%%       (0,0) circle (1);
%%       \draw[color=blue,-{Stealth[length=0.3cm,bend]}]  (0:1) arc (0:315:1);
%%       \draw[color=red,-{Stealth[length=0.3cm,bend]}]  (0:1) arc (0:-45:1);

%%     \end{tikzpicture}
%%   }
%%   \caption{On obtient le même résultat avec des flèches tournantes de sens opposé
%%     pour des valeurs $t$ entières.}
%% \end{figure}
\begin{equation*}
\pmat{\lambda_{\sfrac{-N}{2}} \\ \Vdots \\ \lambda_0 \\ \Vdots \\ \lambda_{\sfrac{N}{2}-1}}
= \frac{1}{N}\pmat{
  \omega_{N}^{\sfrac{-N}{2} \times 0} & \Cdots & \omega_ {N}^{\sfrac{-N}{2} \times (N-1)} \\
  \Vdots & & \Vdots \\
  \omega_{N}^{0 \times 0} & \Cdots & \omega_{N}^{0 \times (N-1)} \\
  \Vdots &  & \Vdots  \\
  \omega_{N}^{(\sfrac{N}{2}-1) \times 0} & \Cdots & \omega_{N}^{(\sfrac{N}{2}-1) \times (N-1)} \\
}
\pmat{x_0 \\ \Vdots \\ \\ \Vdots \\ x_{N-1}}
\end{equation*}
Alors on peut facilement voir que cette manière de calculer les $\lambda_j$ prend un nombre de calculs proportionel
à $N^2$. En effet, il y a $N$ lambdas à calculer,
et pour chacun il faut calculer une somme de $N$ éléments.

\newpage
\subsection{Transformée de Fourier rapide}

Il existe cependant des méthodes plus rapides, notamment l'algorithme de Cooley-Tukey,
du type «diviser pour régner», qui sépare le problème récursivement en deux:
\footnote{En réalité il ne s'agit que du cas le plus simple de cet algorithme,
qui dans sa généralité peut diviser une transformation de Fourier de longueur composée $n = n_1 \cdot n_2$
en $n_1$ transformations de Fourier de longueur $n_2$.}
\label{transfourap}
\begin{equation*}
  \begin{aligned}
    \lambda_j &= \frac{1}{N}\sum_{m=0}^{N-1} \omega_{N}^{jm}x_m \\
&=\frac{1}{N}\sum_{m=0}^{\sfrac{N}{2}-1} \omega_{N}^{j2m}x_{2m}
+ \frac{1}{N}\sum_{m=0}^{\sfrac{N}{2}-1} \omega_{N}^{j(2m+1)}x_{2m+1} \\
&=\frac{1}{N}\sum_{m=0}^{\sfrac{N}{2}-1} \omega_{\sfrac{N}{2}}^{jm}x_{2m}
+ \frac{1}{N}\omega_{N}^j \sum_{m=0}^{\sfrac{N}{2}-1} \omega_{\sfrac{N}{2}}^{jm}x_{2m+1} \\
\lambda_{j+\sfrac{N}{2}} &= \frac{1}{N}\sum_{m=0}^{N-1} \omega_{N}^{(j+\sfrac{N}{2})m}x_m \\
&=\frac{1}{N}\sum_{m=0}^{\sfrac{N}{2}-1} \omega_{N}^{(j+\sfrac{N}{2})2m}x_{2m}
+ \frac{1}{N}\sum_{m=0}^{\sfrac{N}{2}-1} \omega_{N}^{(j+\sfrac{N}{2})(2m+1)}x_{2m+1} \\
&=\frac{1}{N}\sum_{m=0}^{\sfrac{N}{2}-1}\underbrace{\omega_{N}^{Nm}}_{=1} \omega_{N}^{j2m}x_{2m}
+ \frac{1}{N}\omega^{j+\sfrac{N}{2}}_{N}\sum_{m=0}^{\sfrac{N}{2}-1} \omega_{N}^{(j+N)2m}x_{2m+1} \\
&=\frac{1}{N}\sum_{m=0}^{\sfrac{N}{2}-1} \omega_{\sfrac{N}{2}}^{jm}x_{2m}
- \frac{1}{N}\omega^{j}_{N}\sum_{m=0}^{\sfrac{N}{2}-1} \omega_{\sfrac{N}{2}}^{jm}x_{2m+1} \\
    \end{aligned}
  \end{equation*}
  En utilisant la notation introduite à la page \pageref{renotation}, on obtient:

  %[2em]
  $$\begin{aligned} \displaystyle\pa{\mathcal{F}_N\pmat{x_0 \\ \vdots \\ x_{N-1}}}_j
  &= \frac{1}{2} \pa{\mathcal{F}_{\sfrac{N}{2}}\pmat{x_0 \\ x_2\\ \vdots \\ x_{N-2}}}_j
  + \frac{1}{2} \omega_{N}^j \pa{\mathcal{F}_{\sfrac{N}{2}}\pmat{x_1 \\ x_3\\ \vdots \\ x_{N-1}}}_j \\
  \pa{\mathcal{F}_N\pmat{x_0 \\ \vdots \\ x_{N-1}}}_{j+\sfrac{N}{2}}
  &= \frac{1}{2} \pa{\mathcal{F}_{\sfrac{N}{2}}\pmat{x_0 \\ x_2\\ \vdots \\ x_{N-2}}}_j
  - \frac{1}{2} \omega_{N}^j \pa{\mathcal{F}_{\sfrac{N}{2}}\pmat{x_1 \\ x_3\\ \vdots \\ x_{N-1}}}_j
\end{aligned}$$ 
On applique ce processus récursivement. On a donc $\log_2(N)$ étapes, et dans la $j^\text{ème}$ étape,
il y a $N/2^{j}$ termes $\mathcal{F}_{2^{j}}$ à calculer, avec chacun $2^{j}$
composantes:

\begin{figure}[h!]
  \centering
  \begin{forest}
    label tree,
    %forked edges,
    for tree={
        grow=0,reversed, % tree direction
        parent anchor=east,child anchor=west, % edge anchors
        edge={line cap=round},outer sep=+1pt, % edge/node connection
        rounded corners,minimum width=5mm,minimum height=2mm, % node shape
        l sep=9mm % level distance
    }
    [$\displaystyle\mathcal{F}_8\pnnmat{x_0 \\ x_1 \\ x_2 \\ x_3 \\ x_4 \\ x_5 \\ x_6 \\ x_7 \\ x_8}$
    ,level label=étape 3
    [$\displaystyle\mathcal{F}_4\pnnmat{x_0 \\ x_2 \\ x_4 \\ x_6}$
    ,level label=étape 2
    [$\displaystyle\mathcal{F}_2\pnnmat{x_0 \\ x_4}$
    ,level label=étape 1
    [$\mathcal{F}_1 (x_0) \equal x_0$,level label=étape 0]
    [$\mathcal{F}_1 (x_4) \equal x_4$]
    ]
    [$\displaystyle\mathcal{F}_2\pnnmat{x_2 \\ x_6 }$
    [$\mathcal{F}_1 (x_2) \equal x_2$]
    [$\mathcal{F}_1 (x_6) \equal x_6$]
    ]
    ]
    [$\displaystyle\mathcal{F}_4\pnnmat{x_1 \\ x_3 \\ x_5 \\ x_7}$
    [$\displaystyle\mathcal{F}_2\pnnmat{x_1 \\ x_5}$
    [$\mathcal{F}_1 (x_1) \equal x_1$]
    [$\mathcal{F}_1 (x_5) \equal x_5$]
    ]
    [$\displaystyle\mathcal{F}_2\pnnmat{f_8^3 \\ f_8^7 }$
    [$\mathcal{F}_1 (x_3) \equal x_3$]
    [$\mathcal{F}_1 (x_7) \equal x_7$]
    ]
    ]]
\end{forest}
\caption{Processus récursif pour $N = 8$}
\end{figure}

\vfill
Je vais maintenant tenter d'expliquer mon implémentation de la transformation de Fourier
dans le language de programmation Python:
\newpage
\begin{figure}[h]
  \centering
\begin{code}
import numpy as np
def fft(x):
    if len(x) == 1:
        return [x[0]]
    elif len(x) % 2 == 0:
        result = [0]*len(x)
        a = fft(x[::2])
        b = fft(x[1::2])
        for k in range(-len(x)//2,0):
            result[k] = a[k]/2 + np.exp(-2j*np.pi*k/len(x))*b[k]/2
            result[k+len(x)//2] = a[k]/2 - np.exp(-2j*np.pi*k/len(x))*b[k]/2
        return result
    else: # transformation lente
        result = [0]*len(x)
        for k in range(len(x)):
            for l in range(len(x)):
                result[k] += np.exp(-2j*np.pi*k*l/len(x)) * x[l] / len(x)
        return result
\end{code}
\caption{Code Python pour la transformée de Fourier rapide}
\end{figure}
À la ligne 1, j'importe une librairie nommée \textsc{numpy},
contenant différents outils mathématiques et statistiques, notamment la fonction \mintinline{Python}{exp()},
pour l'exponentielle.

À la ligne 2, je commence à definir la fonction de transformation
de Fourier rapide, qui prend comme entrée
une liste de nombres complèxes (p. ex. les points par où il faut passer).

Aux lignes 3 et 4, s'il n'y a qu'un seul point, alors pour passer par celui-ci, il nous faut qu'une seule flèche,
qui ne tourne pas et de position (initiale et permanente) exactement égale à ce point. La fonction \textsc{FFT}
renvoie donc exactement son entrée, une liste de longeur 1.

Sinon, on crée une liste (nommée \mintinline{Python}{result}) donnant les positions initiales
pour chaque type de flèche (aux lignes 6 et 14).

Si la liste d'entrée \textsc{x} est de longueur paire, alors on calcule la transformée
grâce à l'algorithme de Cooley-Tukey décrit à la page \pageref{transfourap}, utilisant la récursivité. Dans Python, \mintinline{Python}{x[a:b:c]},
renvoie une liste contenant chaque \textsc{c} ième élément de la liste \textsc{x} à partir du \text{a} ième
élément jusqu'au \textsc{b} ième élément au maximum, non compris.
S'ils ne sont pas spécifiés par l'utilisateur, alors par défaut \textsc{a = 0} (l'élément au début de la liste), \textsc{b = -1}
(le dernier élément de la liste), et \textsc{c = 1} (le pas est de 1).

Si la liste d'entrée \textsc{x} est de longueur impaire, alors il n'est pas possible d'utiliser cet algorithme,
alors on recourt à la transfomation lente.


Remarque: Python utilise la convention utilisée par les ingénieurs électriques, \mintinline{Python}{j} est
l'unité imaginaire, avant laquelle il faut mettre un nombre pour que Python ne l'interprète pas comme une variable.

\newpage

%%  \subsection{Propriétés de la transformation dans certains cas}
%% \begin{properties}\label{proptransfour}Notons \\$\Vec{f} := \pmat{f_N^0 & \dots & f_N^{N-1}}^\top$ \vspace{0.2em} \begin{itemize}[leftmargin=*]
%%     \item Si $f$ est réele, alors \begin{equation*}\begin{aligned}&\pa{\mathcal{F}_N\pa{\Vec{f}}}_{-j} = \overline{\pa{\mathcal{F}_N\pa{\Vec{f}}}_{j}} \\ &\text{Donc } \pa{\mathcal{F}_N\pa{\Vec{f}}}_{-j} e^{-ij\frac{t}{N}2\pi}\\
%%       &+ \pa{\mathcal{F}_N\pa{\Vec{f}}}_{j} e^{ij\frac{t}{N}2\pi} \\
%%       &= 2\cdot \mathrm{Re}\pa{\pa{\mathcal{F}_N\pa{\Vec{f}}}_{j} e^{ij\frac{t}{N}2\pi}} \\
%%       &= 2 \cdot \mathrm{Re}\pa{\mathcal{F}_N\pa{\Vec{f}}}_j\cdot\cos\pa{j\frac{t}{N}2\pi} \\
%%       &- 2 \cdot \mathrm{Im}\pa{\mathcal{F}_N\pa{\Vec{f}}}_j\cdot\sin\pa{j\frac{t}{N}2\pi}\end{aligned}\end{equation*}
%%     \item Si $f$ est paire, alors \begin{equation*}\begin{aligned}&\pa{\mathcal{F}_N\pa{\Vec{f}}}_{-j} = \pa{\mathcal{F}_N\pa{\Vec{f}}}_{j}\\
%%       &\text{Donc } \pa{\mathcal{F}_N\pa{\Vec{f}}}_{-j} e^{-ij\frac{t}{N}2\pi}\\
%%       &+ \pa{\mathcal{F}_N\pa{\Vec{f}}}_{j} e^{ij\frac{t}{N}2\pi} \\
%%       &= 2\cdot \pa{\mathcal{F}_N\pa{\Vec{f}}}_{j} \cdot\cos\pa{j\frac{t}{N}2\pi} \end{aligned}\end{equation*}
%%       \item Si $f$ est impaire, alors \begin{equation*}\begin{aligned}&\pa{\mathcal{F}_N\pa{\Vec{f}}}_{-j} = -\pa{\mathcal{F}_N\pa{\Vec{f}}}_{j}\\
%%       &\text{Donc } \pa{\mathcal{F}_N\pa{\Vec{f}}}_{-j} e^{-ij\frac{t}{N}2\pi}\\
%%       &+ \pa{\mathcal{F}_N\pa{\Vec{f}}}_{j} e^{ij\frac{t}{N}2\pi} \\
%%       &= 2\cdot \pa{\mathcal{F}_N\pa{\Vec{f}}}_{j} \cdot i\sin\pa{j\frac{t}{N}2\pi} \end{aligned}\end{equation*}
%%     \end{itemize}\end{properties}

\clearpage
\subsection{Transformée inverse de Fourier rapide}
On peut aussi calculer plus rapidement le signal  $f(t)$ à partir des fréquences $\lambda_k$,
en utilisant un algorithme très similaire:

\begin{equation*}
\begin{aligned}
  f(t) &= \sum_{k=\sfrac{-N}{2}}^{\sfrac{N}{2}-1} \lambda_k e^{i k \frac{t}{N} 2\pi} \\
  &= \sum_{k=\sfrac{-N}{4}}^{\sfrac{N}{4}-1} \lambda_{2k} e^{i 2k \frac{t}{N} 2\pi}
  + \sum_{k=\sfrac{-N}{4}}^{\sfrac{N}{4}-1} \lambda_{2k+1} e^{i (2k+1) \frac{t}{N} 2\pi} \\
  &= \sum_{k=\sfrac{-N}{4}}^{\sfrac{N}{4}-1} \lambda_{2k} e^{i k \frac{t}{N/2} 2\pi}
  + e^{\frac{it2\pi}{p}} \sum_{k=\sfrac{-N}{4}}^{\sfrac{N}{4}-1} \lambda_{2k+1} e^{i k \frac{t}{N/2} 2\pi} \\
  f\pa{t+\frac{p}{2}} &= \sum_{k=\sfrac{-N}{2}}^{\sfrac{N}{2}-1} \lambda_k e^{i k \frac{t + N/2}{p} 2\pi} \\
  &= \sum_{k=\sfrac{-N}{4}}^{\sfrac{N}{4}-1} \lambda_{2k} e^{i 2k \frac{t + N/2}{p} 2\pi}
  + \sum_{k=\sfrac{-N}{4}}^{\sfrac{N}{4}-1} \lambda_{2k+1} e^{i (2k+1) \frac{t + N/2}{p} 2\pi} \\
  &= \sum_{k=\sfrac{-N}{4}}^{\sfrac{N}{4}-1} \lambda_{2k} e^{i 2k \frac{t}{N} 2\pi}
  + e^{\frac{i(t+p/2)2\pi}{p}} \sum_{k=\sfrac{-N}{4}}^{\sfrac{N}{4}-1} \lambda_{2k+1} e^{i 2k \frac{t + N/2}{p} 2\pi} \\
  &= \sum_{k=\sfrac{-N}{4}}^{\sfrac{N}{4}-1} \lambda_{2k} e^{i k \frac{t}{N/2} 2\pi}
  - e^{\frac{it2\pi}{p}} \sum_{k=\sfrac{-N}{4}}^{\sfrac{N}{4}-1} \lambda_{2k+1} e^{i k \frac{t}{N/2} 2\pi} \\
\end{aligned}
\end{equation*}
Choisissons $p=N$ et notons $$\pa{\mathcal{F}_N^{-1} \pmat{\lambda_{\sfrac{-N}{2}} \\ \vdots \\ \lambda_{\sfrac{N}{2}-1}}}_t
= \sum_{k=\sfrac{-N}{2}}^{\sfrac{N}{2}-1} \lambda_k e^{ik\frac{t}{N}2\pi} \text{ avec $t \in \cpa{0,\dots,N-1}$}$$
Alors nous obtenons 
\begin{equation*}
  \begin{aligned}
    \pa{\mathcal{F}_N^{-1} \pmat{\lambda_{\sfrac{-N}{2}} \\ \vdots \\ \lambda_{\sfrac{N}{2}-1}}}_t
    &= \pa{\mathcal{F}_{\sfrac{N}{2}}^{-1} \pmat{\lambda_{\sfrac{-N}{2}} \\ \lambda_{\sfrac{-N}{2}+2} \\ \vdots \\ \lambda_{\sfrac{N}{2}-2}}}_t
    +  e^{\frac{it2\pi}{N}}\pa{\mathcal{F}_{\sfrac{N}{2}}^{-1} \pmat{\lambda_{\sfrac{-N}{2}+1} \\ \lambda_{\sfrac{-N}{2}+3} \\ \vdots \\ \lambda_{\sfrac{N}{2}-1}}}_t\\
    \pa{\mathcal{F}_N^{-1} \pmat{\lambda_{\sfrac{-N}{2}} \\ \vdots \\ \lambda_{\sfrac{N}{2}-1}}}_{t+\sfrac{N}{2}}
    &= \pa{\mathcal{F}_{\sfrac{N}{2}}^{-1} \pmat{\lambda_{\sfrac{-N}{2}} \\ \lambda_{\sfrac{-N}{2}+2} \\ \vdots \\ \lambda_{\sfrac{N}{2}-2}}}_t
    -  e^{\frac{it2\pi}{N}}\pa{\mathcal{F}_{\sfrac{N}{2}}^{-1} \pmat{\lambda_{\sfrac{-N}{2}+1} \\ \lambda_{\sfrac{-N}{2}+3} \\ \vdots \\ \lambda_{\sfrac{N}{2}-1}}}_t\\
  \end{aligned}
\end{equation*}

La transformée inverse (rapide) de Fourier est donc très similaire à la transformée (rapide) de Fourier.
Par ailleurs, la matrice suivante est unitaire:
($A\overline{A}^\top=\text{Id}$):
$$A = \frac{1}{\sqrt{N}}\pmat{\omega_N^{\sfrac{-N}{2}\times0} & \Cdots &\omega_N^{\sfrac{-N}{2}\times(N-1)} \\
\Vdots & \Ddots & \Vdots \\
\omega_N^{(\sfrac{N}{2}-1)\times0} & \Cdots & \omega_N^{(\sfrac{N}{2}-1)\times(N-1)} }$$

\begin{figure}[h]
  \centering
\begin{code}
import numpy as np
def ifft(y):
    if len(y) == 1:
        return [y[0]]
    else:
        result = [0]*len(y)
        a = ifft(y[::2])
        b = ifft(y[1::2])
        for k in range(-len(y)//2,0):
            result[k] = a[k] + np.exp(2j*np.pi*k/len(y))*b[k]
            result[k+len(y)//2] = a[k] - np.exp(2j*np.pi*k/len(y))*b[k]
        return result
    else: # transformation lente
       result = [0j] * len(y)
       for k in range(len(y)):
           for l in range(len(y)):
               result[k] += y[l] * np.exp(2j*np.pi*k*l/len(y))
       return result
\end{code}
\caption{Code Python pour la transformée inverse de Fourier rapide \mintinline{Python}{y[k]} $= \lambda_k$}
\end{figure}

\newpage
\subsection{Convolution}
Dans une salle, il y a des échos. Soit $f(t)$ le son émis par quelqu'un, et $g(t)$
la proportion de l'amplitude à laquelle on réentend le son après $t$ secondes. Supposons
que les échos arrivent des multiples d'une seconde plus tard.
Le son total $r(t)$ peut être écrit comme $$r(t) = f(t)\underbrace{g(0)}_{=1} + f(t-1)g(1) + f(t-2)g(2) + \dots$$
$g(t) = 0$ $\forall t < 0$ car on n'entend pas à l'avance.
En réalité, la majorité des échos n'arrivent pas des multiples d'une seconde plus tard,
et donc on doit écrire $$r(t) = \int_{-\infty}^{\infty} f(t-\tau)g(\tau) d\tau
\approx \sum_{\upsilon=-\infty}^\infty T \cdot f(t - \upsilon T)g(\upsilon T)$$

où $T = 1/\textit{fs}$ où $\textit{fs}$ est le nombre d'échantillons par seconde.
Pour un signal $f(t)$ fini de durée $d_1$, on choisit $d_2$ la durée maximale de la réverbation
pour que $d_1 + d_2 = N \cdot T$ avec $N$ une puissance de $2$. Pour tout $t \in [d_1,T \cdot N[$
on met des zéros pour $f(t)$ et pour tout $t \in ]-\infty,0[\cup[d_2,\infty[$ on met
des zéros pour $g(t)$. On rend la fonction $f(t)$ periodique de periode $N \cdot T$. Alors
$$r(t) \approx \sum_{\upsilon=0}^{N-1} T \cdot f(t - \upsilon T)g(\upsilon T)$$
Le nombre de calculs pour directement calculer les $r(t)$ avec $t \in \{0,\dots,(N-1)T\}$
est proportionnel à $N^2$. Mais il est possible de faire mieux, grâce au théorème
de convolution:

\begin{theorem}[Convolution]
  \begin{equation*} \begin{aligned}
    &\pa{\mathcal{F}_N\pmat{r(0) \\ \Vdots \\ r((N-1)T)}}_j \\
    &= T \cdot N \cdot \pa{\mathcal{F}_N\pmat{f(0) \\ \Vdots \\ f((N-1)T)}}_j
    \cdot \pa{\mathcal{F}_N\pmat{g(0) \\ \Vdots \\ g((N-1)T)}}_j
  \end{aligned} \end{equation*}
\end{theorem}
\begin{proofj}
  \begin{equation*} \begin{aligned}
      &\pa{\mathcal{F}_N\pmat{r(0) \\ \Vdots \\ r((N-1)T)}}_j
      = \frac{1}{N} \sum_{m=0}^{N-1} \omega_N^{jm} r(mT) \\
      &= \frac{1}{N} \sum_{m=0}^{N-1} \omega_N^{jm} \sum_{\upsilon=0}^{N-1} T \cdot f(mT - \upsilon T)g(\upsilon T)
      \\
      &= T \cdot \sum_{\upsilon=0}^{N-1} \omega_N^{j \upsilon} g(\upsilon T) \underbrace{\frac{1}{N} \sum_{m=0}^{N-1} \omega_N^{j(m-\upsilon)}f(mT - \upsilon T)}_{\pa{\mathcal{F}_N\pmat{f(0) \\ \Vdots \\ f((N-1)T)}}_j \text{\tiny par périodicité de $f$}}
      \\
      &= T \cdot N \cdot \frac{1}{N} \sum_{\upsilon=0}^{N-1} \omega_N^{j \upsilon} g(\upsilon T) \cdot \pa{\mathcal{F}_N\pmat{f(0) \\ \Vdots \\ f((N-1)T)}}_j \\
      &= T \cdot N \cdot \pa{\mathcal{F}_N\pmat{f(0) \\ \Vdots \\ f((N-1)T)}}_j \cdot \pa{\mathcal{F}_N\pmat{g(0) \\ \Vdots \\ g((N-1)T)}}_j 
  \end{aligned} \end{equation*}
\end{proofj}
Ainsi, pour calculer la convolution de $f$ et $g$, on procède trois étapes:
\begin{enumerate}
\item Calculer $\mathcal{F}(f)$ et $\mathcal{F}(g)$. Cela prend un nombre de calcul proportionnel à $N \cdot \log_2(N)$.
  \item Multiplier chaque composante de $\mathcal{F}(f)$ par la même composante de $\mathcal{F}(g)$ (et $T \cdot N$)
  pour obtenir la composante respective de $\mathcal{F}(r)$, ce qui prend un nombre de calculs proportionnel à $N$, puisqu'il y a $N$ composantes.
\item Calculer la transformation de Fourier inverse du résultat de la deuxième étape. Cela prend un nombre de calcul proportionnel à $N \cdot \log_2(N)$
  \end{enumerate}
\begin{figure*}
\input{programmes/echo.pgf}
\end{figure*}



\newpage
